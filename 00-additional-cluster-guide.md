# Вход на кластер

### 1. Подготовка 

Если вы работаете на Linux, добавьте в `/etc/hosts` такие строки:
```
127.0.0.1       sber-master.atp-fivt.org sber-master
127.0.0.1       sber-node01.atp-fivt.org sber-node01
127.0.0.1       sber-node02.atp-fivt.org sber-node02
127.0.0.1       sber-node03.atp-fivt.org sber-node03
```

Если вы работаете на Windows, поставьте себе [Git](https://git-scm.com/downloads) зайдите в Git Bash. Эта оболочка даст возможность выполнять команды в Linux подобном терминале. 

### 2. Проверка доступа

Для доступа с пробросом необходимых портов используйте команду:

```bash
ssh USER@sber-client.atp-fivt.org \
-L 8088:sber-master.atp-fivt.org:8088 \
-L 8889:sber-master.atp-fivt.org:8889 \
-L 19888:sber-master.atp-fivt.org:19888 \
-L 18088:sber-master.atp-fivt.org:18088 \
-L 8888:sber-node01.atp-fivt.org:8888 \
-L 9870:sber-master.atp-fivt.org:9870 \
-L 8042:sber-node01.atp-fivt.org:8041 \
-L 8043:sber-node02.atp-fivt.org:8041 \
-L 8044:sber-node03.atp-fivt.org:8041 \
-L 8045:sber-node01.atp-fivt.org:8042 \
-L 8046:sber-node02.atp-fivt.org:8042 \
-L 8047:sber-node03.atp-fivt.org:8042
```

Вместо `USER` используйте логин, который пришел вам на почту с адреса automation@atp-fivt.org.
